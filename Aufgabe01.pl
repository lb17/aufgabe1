/**
Input: Liste
Output: �bersichliche ausgabe einer Liste
Funktion: Schreibt jedes Element der Liste in eine �bersichtliche Ansicht und verhidert das Ende von Abfragen obwohl es noch m�glihe Ergebnisse gibt.
*/
listenausgabe([]).
listenausgabe([KOPF|REST]) :- writeln(KOPF), listenausgabe(REST).

/**
Input: Nichts
Output: Collection
Funktion: Das Attribut sammelt alle Tietel und die dazugeh�rigen Tonarten und gibt diese als Tupel in einer Liste aus, am Ende wird die Liste Sortiert.
Beschreibung des Angorithmus: Section 1.1.2 im Entwurf.
*/

collect_stueck(COLLECTION) :-
                            findall((TITEL,TONART), (stueck(SNR,KNR,TITEL,TONART,_), TONART \== null, TITEL \== null, SNR \== null, KNR \== null), UNSORTCOLLECTION),
                            list_to_set(UNSORTCOLLECTION, COLLECTION).
/**
Input: Nichts
Output: Collection
Funktion: Das Pr�dikat sammelt alle CD Nummern und die dazugeh�rige Gesamtspielzeit, wo die Gesamtspielzeit zwischen 88 und 343 liegt und gibt diese als Tupel in einer Liste aus.
Beschreibung des Angorithmus: Section 1.1.3 im Entwurf.
*/
collect_gs(Collection) :-
                       findall((CDNR, GESAMTSPIELZEIT), (cd(CDNR,_,_,_,GESAMTSPIELZEIT), CDNR \== null, GESAMTSPIELZEIT \== null, GESAMTSPIELZEIT < 343, GESAMTSPIELZEIT > 87), Collection).

/**
Input: KListe: Eine Liste mit den Namen von Komponisten
Output: Collection
Funktion: Das Pr�dikat sammelt Namen und die dazugeh�rign Titel von allen Komponisten, die in der KListe vorkmmen und gibt diese als Tupel in einer Liste aus, welche am Ende soritert wird.
Beschreibung des Angorithmus: Section 1.1.4 im Entwurf.
*/
collect_st(KListe,Collection) :-
                           findall((NAME, TITEL), (NAME = X, member(X, KListe), komponist(KPNR,_,NAME,_,_), stueck(_,KPNR,TITEL,_,_)), Collection_unsort),
                           list_to_set(Collection_unsort,Collection).
/**
Input: Instrument
Output: Collection
Funktion: Das Pr�dikat sammelt Vornamen und Namen von Komponisten, die ein St�ck geschrieben haben, in dem das eingegebene Instrument soielt und gibt diese als Tupel in einer Liste aus, welche am Ende soritert wird.
Beschreibung des Angorithmus: Section 1.1.5 im Entwurf.
*/
collect_komp(Instrument,Collection) :-
                                     findall((VORNAME, NAME), (komponist(KNR,VORNAME,NAME,_,_), stueck(SNR,KNR,_,_,_), solist(_,SNR,_,Instrument)),Collection_unsort),
                                     list_to_set(Collection_unsort,Collection).
/**
Input: Nichts
Output: Collection
Funktion: Das Pr�dikat sammelt Vornamen, Namen von Komponisten und die Spielzeit von allen St�cken, die der jewailige Komponis geschrieben hat und gibt diese als Tupel in einer Liste aus, welche am Ende soritert wird.
F�r diese Abfrage, verwenden wir zwei Hilfspr�dikate, welche eine Liste von Gesammspielzeiten erstellt und eins welches diese Liste zusammenrechnet.
Beschreibung des Angorithmus: Section 1.1.6 im Entwurf.
*/
collect_time(COLLECTION) :-
                         findall((VORNAME, NAME, ZEIT),(komponist(KNR,VORNAME,NAME,_,_), KNR \== null, NAME\== null, VORNAME\== null, create_gsz_liste(NAME,ZEIT)),COLLECTION).

create_gsz_liste(NAME, COLLECTION) :-
                               findall(CDNR, (komponist(KNR,VORNAME,NAME,_,_), NAME\== null, VORNAME\== null, stueck(SNR,KNR,_,_,_), SNR \== null, aufnahme(CDNR,SNR,_,_)),CDNR_LISTE),
                               list_to_set(CDNR_LISTE, CDNR_NLISTE),
                               findall(GSZ,(member(CDNR_NEW, CDNR_NLISTE), cd(CDNR_NEW,_,_,_,GSZ)),GSZ_LISTE),
                               zusammenrechung(GSZ_LISTE, COLLECTION).

zusammenrechung([KOPF|REST],Y):-
                            zusammenrechung(REST,X), Y is (KOPF + X).
                            zusammenrechung([],X) :- X = 0.
/**
   Input(Person)
   Output(Vorfahre)
   Das Pr�dikat findet alle Vorfahre Y der Person X, dies machen wir rekursiv.
   Bescheibung des Algorithmus: Section 1.2.1 im Entwurf.
*/

vorfahre(Vorfahre,Person) :-
    elternteil(Vorfahre,Person).

vorfahre(Vorfahre,Person) :-
    elternteil(Elternteil,Person),
    vorfahre(Vorfahre,Elternteil).
/**
   Input(Elternteil)
   Output(Nachkomme)
   Das Pr�dikat findet alle Nachkomme Y der Person X, dies machen wir rekursiv.
   Bescheibung des Algorithmus: Section 1.2.2 im Entwurf.
*/
nachkomme(Nachkomme,Elternteil) :-
    elternteil(Elternteil,Nachkomme).

nachkomme(Nachkomme,Elternteil) :-
    elternteil(Elternteil,Person),
    nachkomme(Nachkomme,Person).
/**
   Input(Elternteil)
   Output(Sortierteliste)
   Das Pr�dikat findet alle Nachkomme Y der Person X, die Nachkomme finden wir mit Hilfe des Pr�dikats nachkomme,
   das Pr�dikat nachkommen gibt uns eine Liste zur�ck die keine Duplikate enth�lt.
   Bescheibung des Algorithmus: Section 1.2.3 im Entwurf.
*/
nachkommen(Elternteil,Sortierteliste):-
                findall(Nachkomme,nachkomme(Nachkomme,Elternteil),Liste),
                sort(Liste,Sortierteliste).
/**
   Input(Personzwei)
   Output(Person)
   Das Pr�dikat eheleute ist ein symmetrischer Pr�dikat, wir haben �ber den Pr�dikat verheiratet das Pr�dikat eheleute symmetrisch gemacht.
   Bescheibung des Algorithmus: Section 1.2.4 im Entwurf.
*/
eheleute(Personeins,Personzwei):-
           verheiratet(Personeins,Personzwei);
           verheiratet(Personzwei,Personeins).

/**
   Input(Eltern)
   Output(Kind)
   Das Pr�dikat kind findet alle Kinder Y von der Person X.
   Bescheibung des Algorithmus: Section 1.2.5 im Entwurf.
*/
kind(Kind,Eltern):-
       eheleute(Eltern,_),
       elternteil(Eltern,Kind).

/**
   Input(Person)
   Output(Geschwister)
   Das Pr�dikat geschwister findet alle Geschwister Y der Person X,
   wir benutzen die Pr�dikaten vater und mutter und sorgen daf�r, dass Person und Geschwister nicht die gleiche Person sind.
   Bescheibung des Algorithmus: Section 1.2.6 im Entwurf.
*/
geschwister(Geschwister,Person):-
               vater(Vater,Geschwister),
               vater(Vater,Person),
               mutter(Mutter,Person),
               mutter(Mutter,Geschwister),
               Person \== Geschwister.
/**
   Input(Person)
   Output(Bruder)
   Das Pr�dikat bruder findet alle Bruder Y der Person X,
   wir benutzen das Pr�dikat geschwister, den wir vorher definiert haben und sorgen daf�r, dass Bruder m�nnlich ist.
   Bescheibung des Algorithmus: Section 1.2.7 im Entwurf.
*/
bruder(Bruder,Person):-
         geschwister(Bruder,Person),
         maennlich(Bruder).
/**
   Input(Person)
   Output(Schwester)
   Das Pr�dikat schwester findet alle Schwester Y der Person X,
   wir benutzen das Pr�dikat geschwister, den wir vorher definiert haben und sorgen daf�r, dass Schwester weiblich ist.
   Bescheibung des Algorithmus: Section 1.2.7 im Entwurf.
*/
schwester(Schwester,Person):-
         geschwister(Schwester,Person),
         weiblich(Schwester).
/**
   Input(Person)
   Output(Grosseltern)
   Das Pr�dikat grosseltern findet alle Gro�eltern Y der Person X,
   Bescheibung des Algorithmus: Section 1.2.8 im Entwurf.
*/
grosseltern(Grosseltern,Person) :-
                      elternteil(Eltern,Person),
                      elternteil(Grosseltern,Eltern).
/**
   Input(Person)
   Output(Opa)
   Das Pr�dikat opa findet alle Opas Y der Person X,
   Bescheibung des Algorithmus: Section 1.2.8 im Entwurf.
*/
opa(Opa,Person):-
          grosseltern(Opa,Person),
          maennlich(Opa).
/**
   Input(Person)
   Output(Oma)
   Das Pr�dikat oma findet alle Omas Y der Person X,
   Bescheibung des Algorithmus: Section 1.2.8 im Entwurf.
*/
oma(Oma,Person):-
          grosseltern(Oma,Person),
          weiblich(Oma).

/**
   Input(Person)
   Output(Uroma)
   Das Pr�dikat uroma findet alle Uromas Y der Person X,
   das vorher definierte Pr�dikat grosseltern wird benutzt und das Pr�dikat mutter.
   Bescheibung des Algorithmus: Section 1.2.9 im Entwurf.
*/
uroma(Uroma,Person) :-
                    grosseltern(Grosseltern,Person),
                    mutter(Uroma,Grosseltern).
/**
   Input(Person)
   Output(Uropa)
   Das Pr�dikat grosseltern findet alle Uropas Y der Person X,
   das vorher definierte Pr�dikat grosseltern wird benutzt und das Pr�dikat vater.
   Bescheibung des Algorithmus: Section 1.2.9 im Entwurf.
*/
uropa(Uropa,Person) :-
              grosseltern(Grosseltern,Person),
              vater(Uropa,Grosseltern).

/**
   Input(Person)
   Output(Mutter)
   Das Pr�dikat mutter findet die Mutter Y der Person X,
   wir benutzen das Pr�dikat elternteil und sorgen daf�r, dass Mutter weiblich ist.
   Bescheibung des Algorithmus: Section 1.2.10 im Entwurf.
*/
mutter(Mutter,Person):-
             elternteil(Mutter,Person),
             weiblich(Mutter).
/**
   Input(Person)
   Output(Vater)
   Das Pr�dikat vater findet den Vater Y der Person X,
   wir benutzen das Pr�dikat elternteil und sorgen daf�r, dass Vater m�nnlich ist.
   Bescheibung des Algorithmus: Section 1.2.10 im Entwurf.
*/
vater(Vater,Person):-
             elternteil(Vater,Person),
             maennlich(Vater).
/**
   Input(Person)
   Output(Tante)
   Das Pr�dikat tante findet die Tanten Y der Person X,
   wir benutzen das Pr�dikat elternteil und das Pr�dikat schwester, f�r die Implementierung von tante.
   Bescheibung des Algorithmus: Section 1.2.11 im Entwurf.
*/
tante(Tante,Person):-
            elternteil(Eltern,Person),
            schwester(Tante,Eltern).
/**
   Input(Person)
   Output(Onkel)
   Das Pr�dikat onkel findet die Onkeln Y der Person X,
   wir benutzen das Pr�dikat elternteil und das Pr�dikat bruder, mit Hilfe dieser zwei Pr�dikate k�nnen wir das Pr�dikat onkel implementieren.
   Bescheibung des Algorithmus: Section 1.2.11 im Entwurf.
*/
onkel(Onkel,Person):-
              elternteil(Eltern,Person),
              bruder(Onkel,Eltern).
/**
   Das Pr�dikat maenUweibl findet alle Personen die m�nnlich und weiblich sind und speichert die in eine Liste, die Liste ohne Duplikate wird ausgegeben.
   Bescheibung des Algorithmus: Section 1.2.12 im Entwurf.
*/
maenUweibl(X):-
            findall(Person,(weiblich(Person),maennlich(Person)),Liste),
            sort(Liste,X).
/**
   Das Pr�dikat verhKor findet alle Tupeln (Mann,Frau) und speichert die in eine Liste, die Tupeln werden in die Liste gespeichert,
   wenn die nicht in der Form (Mann,Frau) in dem Pr�dikat verheiratet gespeichert sind.
   Bescheibung des Algorithmus: Section 1.2.13 im Entwurf.
*/
verhKor(X):-
            findall((Mann,Frau),(verheiratet(Mann,Frau),\+maennlich(Mann),\+weiblich(Frau)),Liste),
            sort(Liste,X).
/**
   Das Pr�dikat elterVoll findet alle Tupeln (Eltern,Kinder) und speichert die in eine Liste, die Tupeln werden in die Liste gespeichert,
   wenn die Personen aus dem Tupel nicht m�nnlich und nicht weiblich sind.
   Bescheibung des Algorithmus: Section 1.2.14 im Entwurf.
*/
elterVoll(X):-
            findall((Eltern,Kinder),(elternteil(Eltern,Kinder),\+maennlich(Eltern),\+weiblich(Eltern),\+maennlich(Kinder),\+weiblich(Kinder)),Liste),
            sort(Liste,X).
/**
   Das Pr�dikat wurzel findet alle Personen und speichert die in eine Liste, die Personen werden in die Liste gespeichert wenn,
   die Person keine Eltern hat, die Liste wird ohne Duplikate ausgegeben.
   Bescheibung des Algorithmus: Section 1.2.15 im Entwurf.
*/
wurzel(X):-
           findall(Person,((maennlich(Person);weiblich(Person)),\+elternteil(_,Person)),Liste),
           sort(Liste,X).