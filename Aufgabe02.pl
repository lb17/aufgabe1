
/**
Input: X
Output: true oder false.
Funktion: Rekursive Definition der nat�rlichen Zahlen.
Beschreibung des Angorithmus: %
*/

nat(0).
nat(s(X)):-
           nat(X).

/**
Input: Zahl
Output: SZahl
Funktion: Das Pr�dikat liefert uns eine SZahl die von einer nat�rlkichen Zahl erzeugt worden ist.
Beschreibung des Angorithmus: Section 1.2.1 im Entwurf.
*/

nat2s(0,0):- !.

nat2s(Zahl,s(SZahl)):-
                      Y = Zahl,
                      A is Y - 1,
                      nat2s(A,SZahl).
/**
Input: SZahl
Output: Zahl
Funktion: Das Pr�dikat liefert uns eine Zahl die von einer SZahl erzeugt worden ist.
Beschreibung des Angorithmus: Section 1.2.2 im Entwurf.
*/

s2nat(0,0):- !.

s2nat(s(SZahl),Zahl):-
                      s2nat(SZahl,A),
                      Zahl is A + 1.
/**
Input: Summand1 und Summand2
Output: Summe
Funktion: Das Pr�dikat liefert uns eine7 SZahl, die SZahl ist die Summe der zwei eingegebennen SZahlen.
Beschreibung des Angorithmus: Section 1.2.3 im Entwurf.
*/

add(0,Summand2,Summand2):-
             nat(Summand2).

add(s(Summand1),Summand2,s(Summe)):-
                   add(Summand1,Summand2,Summe).
/**
Input: Minuend und Subtrahend
Output: Differenz
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist die Differenz der zwei eingegebennen SZahlen.
Beschreibung des Angorithmus: Section 1.2.4 im Entwurf.
*/

sub(0,_,0):- !.
sub(Differenz,0,Differenz):-
                            nat(Differenz).
                            
sub(s(Minuend), s(Subtrahend), Differenz):-
                    sub(Minuend,Subtrahend,Differenz),!.

/**
Input: Faktor1 und Faktor2
Output: Produkt
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Multiplikation der zwei eingegebennen SZahlen.
Beschreibung des Angorithmus: Section 1.2.5 im Entwurf.
*/

mul(0,_,0):- !.
mul(_,0,0):- !.
                        
mul(Faktor1,s(Faktor2),Produkt):-
                                mul(Faktor1,Faktor2,Summe),
                                add(Summe,Faktor1,Produkt),!.

/**
Input: B und E
Output: R
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Exponention der zwei eingegebennen SZahlen.
Beschreibung des Angorithmus: Section 1.2.6 im Entwurf.
*/

power(B,0,s(0)):-
              nat(B),!.

power(B,s(E),R):-
              power(B,E,X),
              mul(X,B,R),!.

/**
Input: N
Output: Factorial
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Fakult�t der zwei eingegebennen SZahlen.
Beschreibung des Angorithmus: Section 1.2.7 im Entwurf.
*/

fac(0,s(0)):- !.

fac(s(N), Factorial):-
                      fac(N,X),
                      mul(X,s(N),Factorial).
/**
Input: N1 und N2
Output: true oder false
Funktion: wenn N1 == N2 return false, wenn N1 < N2 return true, wenn N1 > N2 return false.
Beschreibung des Angorithmus: Section 1.2.8 im Entwurf.
*/

lt(0,0):-
         !,false.
         
lt(0,_):-
         !,true.

lt(_,0):-
         !,false.
         
lt(s(N1), s(N2)):-
       lt(N1,N2).

/**
Input: N1 und N2
Output: true oder false
Funktion: wenn N1 == N2 return true, wenn N1 < N2 return false, wenn N1 > N2 return false, dies ist eine Hilfspr�dikat f�r die L�sung der Aufgabe 2.2.9.
*/

equals(0,0):-
             !,true.
equals(_,0):-
             !,false.
equals(0,_):-
             !,false.
             
equals(s(N1), s(N2)):-
              equals(N1,N2).

/**
Input: A und B
Output: C
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Rest der Division von den zwei eingegebennen SZahlen A und B.
Beschreibung des Angorithmus: Section 1.2.9 im Entwurf.
*/

mods(0,0,0):- !.
mods(_,s(0),0):-!.
mods(_,0,_):-
             !,false.
mods(A,B,0):-
             equals(A,B),!.
mods(A,B,A):-
             lt(A,B),!.
mods(A,B,C):-
             sub(A,B,X),
             mods(X,B,C),!.